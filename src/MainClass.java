import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.IntStream;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class MainClass extends Thread {
	private GUI gui;
	private File f;

	public MainClass(GUI gui, File f) {
		super();
		this.gui = gui;
		this.f = f;
	}

	public void run() {

		long time = System.currentTimeMillis();

		FileReader fileR;
		try {
			fileR = new FileReader(f);// создаем объект для чтения файла

			BufferedReader bufR = new BufferedReader(fileR);// содаем объект для быстрого чтения файла

			int counterOfLine = 0;
			String s = bufR.readLine();

			while ((s = bufR.readLine()) != null) {// считаем количество строк в файле
				counterOfLine++;
			}
			counterOfLine++;
			bufR.close();

			try {

				fileR = new FileReader(f);
				bufR = new BufferedReader(fileR);
				int[] m = new int[counterOfLine];// создаем массив для обработки и сортировки данных
				try {
					int progress = 0;

					for (int i = 0; i < counterOfLine; i++) {
						m[i] = Integer.valueOf(bufR.readLine());// заносим значение строки в массив
						if (counterOfLine > 100) {
							if (progress <= 98) {
								if (i % (counterOfLine / 100) == 0) {// выводим состояние на прогрессБар
									progress += 1;
									final int percent = progress;
									gui.setPercent(percent);
									SwingUtilities.invokeLater(gui);
								}
							}
						}
					}

					Arrays.sort(m);// сортируем массив
					gui.getTextAreaMax().setText(Integer.toString(m[counterOfLine - 1]));
					gui.getTextAreaMin().setText(Integer.toString(m[0]));
					double average = IntStream.of(m).average().getAsDouble();
					gui.getTextAreaAvrg().setText(Double.toString(average));
					gui.setPercent(99);
					SwingUtilities.invokeLater(gui);
					double mediana;// вычисляем медиану
					if (counterOfLine % 2 == 0) {// если число строк парное
						mediana = (m[counterOfLine / 2] + m[(counterOfLine / 2) - 1]) / 2;
					} else {
						mediana = (int) m[(counterOfLine / 2)];

					}
					gui.setPercent(100);
					SwingUtilities.invokeLater(gui);
					gui.getTextAreaMediana().setText(Double.toString(mediana));

					gui.getTextAreaTime().setText(Long.toString(System.currentTimeMillis() - time));
				} catch (FileNotFoundException e1) {

					e1.printStackTrace();
				}
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(gui.getFrame(), "Please choose another file!\n This one does not have numbers.");
			}
		} catch (FileNotFoundException e)

		{
			e.printStackTrace();
		} catch (IOException e2) {

			e2.printStackTrace();
		}

	}

}
