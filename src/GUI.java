import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.UIManager;

public class GUI implements Runnable {
	private int percent;

	private JFrame frame;
	private File file = null;
	private JTextArea textAreaMin;
	private JTextArea textAreaMax;
	private JTextArea textAreaAvrg;
	private JTextArea textAreaMediana;
	private JTextArea textAreaTime;
	private JProgressBar progressBar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	public void updateBar(int newValue) {
		progressBar.setValue(newValue);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 549, 216);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());// добавляем стиль для интерфейса
		} catch (Exception e) {
		}

		getTextAreaMin();
		getTextAreaAvrg();
		getTextAreaMediana();
		getTextAreaMax();
		getTextAreaTime();
		getProgressBar();

		// реакция на событие нажатия сновки Старт
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {// если файл не выбран - выдаем сообщение
				if (file == null) {
					JOptionPane.showMessageDialog(frame, "Please open file!");
				} else { // создаем объект класса для вычисления параметров файла
					MainClass mC = new MainClass(getGUI(), file);
					mC.start();

				}
			}
		});

		btnStart.setBounds(386, 125, 89, 23);
		frame.getContentPane().add(btnStart);
		// реакция на событие нажатия кнопки Открыть файл
		JButton btnFile = new JButton("Open File");
		btnFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileopen = new JFileChooser();// создаем объект файлЧузер
				int ret = fileopen.showDialog(null, "Open File");// открываем диалоговое окно
				if (ret == JFileChooser.APPROVE_OPTION) {
					file = fileopen.getSelectedFile();// открываем файл
					progressBar.setVisible(false);
					textAreaAvrg.setText(null);// обновляем все значения в интерфейсе
					textAreaMax.setText(null);
					textAreaMediana.setText(null);
					textAreaMin.setText(null);
					textAreaTime.setText(null);

				}
			}
		});
		btnFile.setBounds(286, 125, 89, 23);
		frame.getContentPane().add(btnFile);

	}

	public JProgressBar getProgressBar() {
		if (progressBar == null) {
			progressBar = new JProgressBar();
			progressBar.setBounds(286, 151, 189, 14);
			frame.getContentPane().add(progressBar);
			progressBar.setVisible(false);
			progressBar.setStringPainted(true);

		}
		return progressBar;
	}

	public JTextArea getTextAreaTime() {
		if (textAreaTime == null) {
			textAreaTime = new JTextArea();
			textAreaTime.setBounds(21, 125, 222, 40);
			textAreaTime.setBorder(BorderFactory.createTitledBorder("Lead time (ms)"));
			frame.getContentPane().add(textAreaTime);
			textAreaTime.setFont(new Font("Arial", Font.PLAIN, 12));

		}
		return textAreaTime;
	}

	public JTextArea getTextAreaMin() {
		if (textAreaMin == null) {
			textAreaMin = new JTextArea();
			textAreaMin.setBounds(21, 10, 222, 40);
			textAreaMin.setBorder(BorderFactory.createTitledBorder("Min value"));
			frame.getContentPane().add(textAreaMin);
			textAreaMin.setFont(new Font("Arial", Font.PLAIN, 12));

		}
		return textAreaMin;
	}

	public JTextArea getTextAreaMax() {
		if (textAreaMax == null) {
			textAreaMax = new JTextArea();
			textAreaMax.setBounds(21, 55, 222, 40);
			textAreaMax.setBorder(BorderFactory.createTitledBorder("Max value"));
			frame.getContentPane().add(textAreaMax);
			textAreaMax.setFont(new Font("Arial", Font.PLAIN, 12));
		}
		return textAreaMax;
	}

	public JTextArea getTextAreaAvrg() {
		if (textAreaAvrg == null) {
			textAreaAvrg = new JTextArea();
			textAreaAvrg.setBounds(270, 10, 222, 40);
			textAreaAvrg.setBorder(BorderFactory.createTitledBorder("Avarage"));
			frame.getContentPane().add(textAreaAvrg);
			textAreaAvrg.setFont(new Font("Arial", Font.PLAIN, 12));
		}
		return textAreaAvrg;
	}

	public JTextArea getTextAreaMediana() {
		if (textAreaMediana == null) {
			textAreaMediana = new JTextArea();
			textAreaMediana.setBounds(270, 55, 222, 40);
			textAreaMediana.setBorder(BorderFactory.createTitledBorder("Median"));
			frame.getContentPane().add(textAreaMediana);
			textAreaMediana.setFont(new Font("Arial", Font.PLAIN, 12));
		}
		return textAreaMediana;
	}

	public GUI getGUI() {
		return this;
	}

	public JFrame getFrame() {
		return frame;
	}

	@Override
	public void run() {
		progressBar.setVisible(true);
		progressBar.setValue(percent);
		if (percent >= 102)
			progressBar.setVisible(false);
	}

	public void setPercent(int percent) {
		this.percent = percent;
	}
}
